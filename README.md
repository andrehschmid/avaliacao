Avaliação de estágio
====================

A avaliação consiste na criação de um CRUD (Create, Retrieve, Update, Delete) para o domain br.avaliacao.Usuario, utilizando o framework Grails.

## Infra-estrutura ##

A aplicação está utilizando a seguinte infra-estrutura:

* Framework Grails na versão [2.5.1](https://grails.org/download.html);
* JDK 8;
* Banco de dados h2 imbutido do Grails.

Recomendamos utilizar uma IDE com suporte a Grails, como a [GGTS](https://spring.io/tools/ggts/all) (free), ou a [IntelliJ IDEA](https://www.jetbrains.com/idea/download/#section=windows). 

No caso da GGTS, é necessário atualizar compilador groovy da IDE para a versão 2.4 ([Groovy Compiler 2.4, seção Groovy support](https://tedvinke.wordpress.com/2015/04/10/grails-3-released-installing-gradle-and-groovy-2-4-support-in-eclipseggts/)). Também é necessário modificar a versão do grails utilizada na IDE para a 2.5.1. Para isso, deve-se acessar o menu Window-> Preferences-> Groovy -> Grails, clicar em "Add...", selecionar o diretório e clicar em OK.

### Como rodar ###

1. Criar uma conta no Bitbucket. 
2. Fazer um [Fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) para um repositório próprio aberto, para que possa realizar commits das suas modificações. Recomendamos utilizar o [SourceTree](https://www.sourcetreeapp.com/) como client git do bitbucket, para gerenciar o repositório "Forkeado".

3. Configurar o projeto na IDE escolhida.

A aplicação já está pronta para rodar sem necessidade de configuração, utilizando o banco de dados em memória e imbutido, h2 database. Basta executar o comando grails "run-app".

## Objetivo ##

Criar CRUD para domain Usuario, contendo:

* Tela para criação de usuário;
    * com confirmação de password.
* Tela para a edição de usuário;
    * edição de senha opcional, mas, se preenchida, exigir senha antiga, nova e confirmação da nova.
* Tela para a listagem, busca de usuário e remoção de usuário, com paginação. A busca deverá filtrar pelo nome e login do usuário.

### O Domain br.avaliacao.Usuario ###

O Domain de usuário deve armazenar as seguintes informações:

* Nome
    * obrigatório, tamanho máximo de 300 caracteres
* Login
    * obrigatório, tamanho máximo de 20 caracteres, **deve ser único entre os usuários**
* Senha
    * obrigatório, tamanho máximo de 30 caracteres
* E-mail
    * obrigatório, validando se é um e-mail
* Estará associado com um Papel, representado pelo domain br.avaliacao.Papel
    * obrigatório

## Referências ##
https://grails.org/single-page-documentation.html (Página Oficial)

http://grails.asia/grails-tutorial-for-beginners/