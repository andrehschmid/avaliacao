import br.avaliacao.Papel

class BootStrap {

    def init = { servletContext ->
        def roleUser = Papel.findByCodigo('USER')
        if(!roleUser) {
            roleUser = new Papel(nome: 'Usuário', codigo: 'USER')
            if(roleUser.save(flush: true)) {
                log.info('Papel USER criado')
            } else {
                log.info('Falha ao criar o Papel USER')
            }
        }

        def roleAdmin = Papel.findByCodigo('ADMIN')
        if(!roleAdmin) {
            roleAdmin = new Papel(nome: 'Administrador', codigo: 'ADMIN')
            if(roleAdmin.save(flush: true)) {
                log.info('Papel ADMIN criado')
            } else {
                log.info('Falha ao criar o Papel ADMIN')
            }
        }
    }
    def destroy = {
    }
}
